# gigaverb-vst

Port to C++ of Gigaverb plus VST Wrapper. No GUI. Year 2006 aprox.

---

```
= = = = = = = = = = = =
  Gigaverb VST v 1.0
= = = = = = = = = = = =
```

A VST version of the famous minutes-long reverb, just load the plugin dll in your favourite host application and give it a go!



- - - Compiling with MinGW / Dev C++ - - -

Once downloaded and extracted the VST SDK you can put the "AGigaverb" folder into the SDK's samples folder, i.e.:

  `C:\MyVST\vstsdk2.4\public.sdk\samples\vst2.x\`

Then put:
* `Gverb` and `AGigaverb` folders
* `GigaverbVST.dev` and `MyDef.def` files
in a place like

* `C:\MyVST\`

Then load `GigaverbVST.dev` in Dev C++ and you should be done :)

- - - - - -

Original Code by Olaf Matthes - http://www.akustische-kunst.org/maxmsp/

C++ porting & VST by Mathieu Bosi

