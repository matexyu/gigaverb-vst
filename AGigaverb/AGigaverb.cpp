/*
 Original code by Olaf Matthes
 VST version by Mathieu Bosi
*/

#include "AGigaverb.h"


//------------------------------------------------------------------------------
AudioEffect* createEffectInstance (audioMasterCallback audioMaster)
//------------------------------------------------------------------------------
{
	return new AGigaverb (audioMaster);
}


//------------------------------------------------------------------------------
AGigaverb::AGigaverb (audioMasterCallback audioMaster)
//------------------------------------------------------------------------------
: AudioEffectX (audioMaster, kGigaverbNumPrograms, kGigaverbNumParams)	
             // audioMaster, # programs, # parameters
{

	setNumInputs (2);		// stereo in
	setNumOutputs (2);		// stereo out
	setUniqueID ('asda');	// identify
	canProcessReplacing ();	// supports replacing output
	canMono();
	//canDoubleReplacing ();	// supports double precision processing _DO NOT WANT_
	
	curProgram = 0;
	
	m_sampleRate = 44100;
	myGigaverb = new Gigaverb(m_sampleRate);	
	
   programs = new AGigaverbProgram[kGigaverbNumPrograms];
  
  setProgram (0);
	
}

//------------------------------------------------------------------------------
AGigaverb::~AGigaverb ()
//------------------------------------------------------------------------------
{
int i;

if (programs)	delete[] programs;
  
delete myGigaverb;
}



void AGigaverb::resume()
{
 m_sampleRate = (int)AudioEffectX::getSampleRate();
 AudioEffectX::resume();
}


void AGigaverb::setProgram(long program)
{
	AGigaverbProgram * ap = &programs[program];

	curProgram = program;
	
  setParameter( kRoomSize,       ap->RoomSize       );
  setParameter( kReverbTime,     ap->ReverbTime     );
  setParameter( kDamping,        ap->Damping        );
  setParameter( kInputBandwidth, ap->InputBandwidth );
  setParameter( kDryLevel,       ap->DryLevel );
  setParameter( kWetLevel,       ap->WetLevel );
  setParameter( kEarlyLevel,     ap->EarlyLevel );
  setParameter( kTailLevel,	     ap->TailLevel  );
}


//------------------------------------------------------------------------------
void AGigaverb::setProgramName (char* name)
//------------------------------------------------------------------------------
{
	vst_strncpy (programs[curProgram].name, name, kVstMaxProgNameLen);
}

//------------------------------------------------------------------------------
void AGigaverb::getProgramName (char* name)
//------------------------------------------------------------------------------
{
//	vst_strncpy (name, programs[curProgram].name, kVstMaxProgNameLen);
	//if (!strcmp (programs[curProgram].name, "Init"))
		sprintf (name, "%s %d", programs[curProgram].name, curProgram + 1);
	//else
	//	vst_strncpy (name, programs[curProgram].name, kVstMaxProgNameLen);
}

//------------------------------------------------------------------------------
void AGigaverb::setParameter (VstInt32 index, float value)
//------------------------------------------------------------------------------
{
	myGigaverb->SetParameterValue(index, value);
	
	//if (editor)	editor->postUpdate ();	
}

//------------------------------------------------------------------------------
float AGigaverb::getParameter (VstInt32 index)
//------------------------------------------------------------------------------
{
	return myGigaverb->GetParameterValue(index);
}

//------------------------------------------------------------------------------
void AGigaverb::getParameterName (VstInt32 index, char* label)
//------------------------------------------------------------------------------
{
  const char* the_name;
  
  the_name = myGigaverb->GetParameterName(index);
  
	vst_strncpy (label, the_name, kVstMaxParamStrLen);
}

//------------------------------------------------------------------------------
void AGigaverb::getParameterDisplay (VstInt32 index, char* text)
//------------------------------------------------------------------------------
{
myGigaverb->GetParameterDisplay(index, text);
}

//------------------------------------------------------------------------------
void AGigaverb::getParameterLabel (VstInt32 index, char* label)
//------------------------------------------------------------------------------
{
  const char* the_name;
  
  //the_name = myGigaverb->GetParameterName(index);
  the_name = myGigaverb->GetParameterLabel(index);
    
	vst_strncpy (label, the_name, kVstMaxParamStrLen);
}

//------------------------------------------------------------------------------
bool AGigaverb::getEffectName (char* name)
//------------------------------------------------------------------------------
{
	vst_strncpy (name, "Gigaverb VST", kVstMaxEffectNameLen);
	return true;
}

//------------------------------------------------------------------------------
bool AGigaverb::getProductString (char* text)
//------------------------------------------------------------------------------
{
	vst_strncpy (text, "Gigaverb VST v0.1", kVstMaxProductStrLen);
	return true;
}

//------------------------------------------------------------------------------
bool AGigaverb::getVendorString (char* text)
//------------------------------------------------------------------------------
{
	vst_strncpy (text, "Mathieu -bdk- Bosi", kVstMaxVendorStrLen);
	return true;
}

//------------------------------------------------------------------------------
VstInt32 AGigaverb::getVendorVersion ()
//------------------------------------------------------------------------------
{ 
	return 8008; 
}

//------------------------------------------------------------------------------
void AGigaverb::processReplacing (float** inputs, float** outputs, VstInt32 sampleFrames)
//------------------------------------------------------------------------------
{
    float* in1  =  inputs[0];
    float* in2  =  inputs[1];
    float* out1 = outputs[0];
    float* out2 = outputs[1];
    
    myGigaverb->PerformDSPLoop(in1, in2, out1, out2, sampleFrames);
    
/*
    float l, r;
    while (--sampleFrames >= 0)
    {
      l = (*in1++);
      r = (*in2++);
        (*out1++) = l * fGain;
        (*out2++) = r * fGain;
    }
*/
}


//------------------------------------------------------------------------------
void AGigaverb::processDoubleReplacing (double** inputs, double** outputs, VstInt32 sampleFrames)
//------------------------------------------------------------------------------
{
    double* in1  =  inputs[0];
    double* in2  =  inputs[1];
    double* out1 = outputs[0];
    double* out2 = outputs[1];
    
	  double dGain = 0.0;//fGain;

    double l, r;

    while (--sampleFrames >= 0)
    {
      l = (*in1++);
      r = (*in2++);
        (*out1++) = l * dGain;
        (*out2++) = r * dGain;
    }
}

