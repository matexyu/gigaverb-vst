/*
 Original code by Olaf Matthes
 VST version by Mathieu Bosi
*/

#ifndef __AGigaverb_h__
#define __AGigaverb_h__

#include "public.sdk/source/vst2.x/audioeffectx.h"

#include "../Gverb/Gigaverb.h"

#include <stdio.h> // for sprintf()


class AGigaverbProgram {
  
friend class AGigaverb;

public:
	AGigaverbProgram()
  {
    //Beware: these must be normalized values
    RoomSize = 0.5f;
    ReverbTime = 0.1f;
    Damping = 0.3f;
    InputBandwidth = 0.7f;
    DryLevel = 0.9f;
    WetLevel = 0.8f;
    EarlyLevel = 0.6f;
    TailLevel = 1.0f;
    
    //for(int i=0; i < kVstMaxProgNameLen + 1; i++) name[i] = '\0';
    sprintf(name, "---");
  }
  
	~AGigaverbProgram () {}

private:	
  float RoomSize;
  float ReverbTime;
  float Damping;
  float InputBandwidth;
  float DryLevel;
  float WetLevel;
  float EarlyLevel;
  float TailLevel;
	
	char name[kVstMaxProgNameLen + 1];
};



//-------------------------------------------------------------------------------------------------------
class AGigaverb : public AudioEffectX
{
public:
	AGigaverb (audioMasterCallback audioMaster);
	~AGigaverb ();
	
	// resume(), to get the effective samplerate with AudioEffectX::getSampleRate()
	virtual void resume();

	// Processing
	virtual void processReplacing (float** inputs, float** outputs, VstInt32 sampleFrames);
	virtual void processDoubleReplacing (double** inputs, double** outputs, VstInt32 sampleFrames);

	// Program
	virtual void setProgramName (char* name);
	virtual void getProgramName (char* name);
	// @@@ new:
	virtual void setProgram (long program);
	
	// @@@ new:
	virtual VstPlugCategory getPlugCategory () { return kPlugCategEffect; }
	
	

	// Parameters
	virtual void  setParameter (VstInt32 index, float value);
	virtual float getParameter (VstInt32 index);
	virtual void getParameterLabel (VstInt32 index, char* label);
	virtual void getParameterDisplay (VstInt32 index, char* text);
	virtual void getParameterName (VstInt32 index, char* text);

	virtual bool getEffectName (char* name);
	virtual bool getVendorString (char* text);
	virtual bool getProductString (char* text);
	virtual VstInt32 getVendorVersion ();

protected:

	Gigaverb *myGigaverb;
	int m_sampleRate;

  AGigaverbProgram *programs;
  
  long curProgram;

};

#endif
