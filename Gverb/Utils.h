/*
 Original code by Olaf Matthes
 C++ porting by Mathieu Bosi
*/

#ifndef __Utils_h__
#define __Utils_h__


#include "Denorm.h"


// Convert a value in dB's to a coefficent
#define DB_CO(g) ((g) > -90.0f ? pow(10.0f, (g) * 0.05f) : 0.0f)
// and back to dB
#define CO_DB(g) ((g) != 0.0f ? 20.0f/log(10) * log((g)) : -90.0f)

#define CLIP(v,a,b) ( (v) > (b) ? (b) :  (v) < (a) ? (a) : (v)  )


class Utils {

// Truncate float to int
static int ff_trunc(float f)
{
  f -= 0.5f;
  f += (3<<22);
  return *((int*)&f) - 0x4b400000;
}

// Round float to int (faster than f_trunc)
static int ff_round(float f)
{
  f += (3<<22);
  return *((int*)&f) - 0x4b400000;
}

};

#endif

