/*
 Original code by Olaf Matthes
 C++ porting by Mathieu Bosi
*/

#ifndef __Gigaverb_h__
#define __Gigaverb_h__

#include <math.h>

#include "Utils.h"

// Some needed DSP blocks:
#include "FixedDelay.h"
#include "Diffuser.h"
#include "Damper.h"


// Better not to change this one... :)
#define FDNORDER 4


// VST parameters indexing facilities...
enum {
  kGigaverbNumPrograms = 16,
  // - - - - -
  kRoomSize = 0,
  kReverbTime,
  kDamping,
  kInputBandwidth,
  kDryLevel,
  kWetLevel,
  kEarlyLevel,
  kTailLevel,
  
  kGigaverbNumParams
};




class Gigaverb {
  
public:
  
	bool  m_bypass;
	int   m_sampleRate;
	
	float m_inputBandwidth;
	float m_dryLevel;
	float m_wetLevel;
	float m_tailLevel;
	float m_earlyLevel;
	
	// Normalized (VST 0..1) cached values of the above ones:
  float m_dryLevelNorm;
	float m_wetLevelNorm;
	float m_tailLevelNorm;
	float m_earlyLevelNorm;
	
	
	Damper *m_inputDamper;
	
	float m_roomSizeMax;
	float m_roomSize;	
  float m_roomSizeNorm; // between 0..1
  
	float m_reverbTime;
	float m_reverbTimeNorm;
	
	float m_delayMax;
	float m_delayLargest;
	FixedDelay *m_fdnDelays[FDNORDER];
	float m_fdnGains[FDNORDER];
	int   m_fdnLengths[FDNORDER];
	
	Damper *m_fdnDampers[FDNORDER]; 
	float   m_fdnDamping;
	
	Diffuser  *m_diffusersL[FDNORDER]; // 4
	Diffuser  *m_diffusersR[FDNORDER];
	
	FixedDelay *m_tapDelay;
	
	int   m_taps[FDNORDER];
	float m_tapsGains[FDNORDER];
	float m_d[FDNORDER];
	float m_u[FDNORDER];
	float m_f[FDNORDER];
	double m_alpha;
	
	
	Gigaverb(int sample_rate);
	~Gigaverb();
	
	
	void Flush(); //  Set all the internal DSP audio buffers to 0.0
	void Clear(); //	clear the delay lines and other stuff
	
	void PerformDSPLoop(float *in1, float *in2, float *out1, float *out2, int n);
	
  void Do(float x, float *yl, float *yr); // Computation for 1 sample
  void FdnMatrix(float *a, float *b);	// Compute a FDN matrix
  
  // Parameters set Methods:
  void SetRoomSize(double a);
  void SetReverbTime(double a);
  void SetDamping(double a);
  void SetInputBandwidth(double a);
  void SetDryLevel(double a);
  void SetWetLevel(double a);
  void SetEarlyLevel(double a);
  void SetTailLevel(double a);
  
  
  // Let's talk with Steinberg's VST functions...
  const char* GetParameterName(int index);
  const char* GetParameterLabel(int index);
  float GetParameterValue(int index);
  void  SetParameterValue(int index, float a);
  void  GetParameterDisplay(int index, char* text);
  
};

#endif




