/*
 Original code by Olaf Matthes
 C++ porting by Mathieu Bosi
*/

#ifndef __FixedDelay__
#define __FixedDelay__

#include <mem.h> // for memset()

#include "Utils.h"


class FixedDelay {

public:	

  int m_size;
  int m_idx;
  float *m_buf;

void Flush()
{
  memset(m_buf, 0, m_size * sizeof(float));
}


FixedDelay(int size)
{
 m_size = size;
 m_idx = 0;
 m_buf = new float[size];

 for (int i = 0; i < size; i++)	m_buf[i] = 0.0;
}

~FixedDelay()
{
 delete m_buf;
}



float Read(int n)
{
int i;

 i = (m_idx - n + m_size) % m_size;

return( m_buf[i] );
}


void Write(float x)
{
	FIX_DENORM_NAN_FLOAT(x);
	
	m_buf[m_idx] = x;
	m_idx = (m_idx + 1) % m_size;
}


};


#endif

