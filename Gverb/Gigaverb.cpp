/*
 Original code by Olaf Matthes
 C++ porting by Mathieu Bosi
*/

#include "Gigaverb.h"

#include <stdio.h> // for sprintf()


//------------------------------------------------------------------------------
Gigaverb::Gigaverb(int sample_rate)
//------------------------------------------------------------------------------
{
	float room_size_max = 300.0f;
	float room_size     = 100.0f;
	float reverb_time = 60.0f;
	float damping = 0.6f;
	float spread  = 50.0f;
	
	float input_bandwidth = 0.3f;
	float dry_level    = 0.7f; //-1.9832f;
	float wet_level    = 0.7f;
	float early_level  = 0.05f; //-1.9832f;
	float tail_level   = 0.75; // <-- THIS determines the neverending effect :D

	float ga,gb,gt;
	int i,n;
	float r;
	float diffscale;
	int a,b,c,cc,d,dd,e;
	float spread1,spread2;


	m_sampleRate  = sample_rate;
	
	m_bypass = false; // remove bypass maybe ( also from Perform() )
	
	m_fdnDamping  = damping;
	m_roomSizeMax = room_size_max;
	m_roomSize    = CLIP(room_size, 0.1f, room_size_max);
	m_reverbTime  = reverb_time;
	m_dryLevel    = dry_level;
	m_wetLevel    = wet_level;
	m_earlyLevel  = early_level;
	m_dryLevel    = dry_level;
	m_tailLevel   = tail_level;

	m_delayMax     = m_sampleRate * m_roomSizeMax/340.0;
	m_delayLargest = m_sampleRate * m_roomSize/340.0;

	if(m_roomSizeMax != 300.0f) m_roomSizeMax = 300.0f;
		//post("gigaverb~: maximum roomsize: %f", m_roomSizeMax);

	
	//	
	// Input damper:
	//
	m_inputBandwidth = input_bandwidth;
	m_inputDamper = new Damper(1.0 - m_inputBandwidth);


	//
	// FDN section
	//
	for(i = 0; i < FDNORDER; i++)
	{
		m_fdnDelays[i] = new FixedDelay((int)m_delayMax + 1000);
	}
	

	for(i = 0; i < FDNORDER; i++)
	{
		m_fdnDampers[i] = new Damper(m_fdnDamping);
	}

	
  // Compute damping factor:
	ga = 60.0;
	gt = m_reverbTime;
	ga = pow(10.0,-ga/20.0);
	n = (int)(m_sampleRate*gt);
	m_alpha = pow((double)ga, 1.0/(double)n);

	gb = 0.0;
	for(i = 0; i < FDNORDER; i++)
	{
		if (i == 0) gb = 1.000000 * m_delayLargest;
		if (i == 1) gb = 0.816490 * m_delayLargest;
		if (i == 2) gb = 0.707100 * m_delayLargest;
		if (i == 3) gb = 0.632450 * m_delayLargest;

#if 0
		m_fdnLengths[i] = NearestPrime((int)gb, 0.5);
#else
		m_fdnLengths[i] = (int)gb;
#endif
		// m_fdnGains[i] = -pow(m_alpha,(double)m_fdnLengths[i]);
		m_fdnGains[i] = -powf((float)m_alpha, m_fdnLengths[i]);
	}


	//
	// Diffuser section
	//

	diffscale = (float)m_fdnLengths[3] / (210 + 159 + 562 + 410);
	spread1 = spread;
	spread2 = 3.0*spread;

	// Left channel:
	b  = 210;
	r  = 0.125541f;
	a  = (int)(spread1 * r);
	c  = 210 + 159 + a;
	cc = c - b;
	r  = 0.854046f;
	a  = (int)(spread2 * r);
	d  = 210 + 159 + 562 + a;
	dd = d - c;
	e  = 1341 - d;
	
	m_diffusersL[0] = new Diffuser((int)(diffscale*b ), 0.75);
	m_diffusersL[1] = new Diffuser((int)(diffscale*cc), 0.75);
	m_diffusersL[2] = new Diffuser((int)(diffscale*dd), 0.625);
	m_diffusersL[3] = new Diffuser((int)(diffscale*e ), 0.625);

	
  // Right channel:
	b  = 210;
	r  = -0.568366f;
	a  = (int)(spread1 * r);
	c  = 210 + 159 + a;
	cc = c - b;
	r  = -0.126815f;
	a  = (int)(spread2 * r);
	d  = 210 + 159 + 562 + a;
	dd = d - c;
	e  = 1341 - d;
	
	m_diffusersR[0] = new Diffuser((int)(diffscale*b ), 0.75);
	m_diffusersR[1] = new Diffuser((int)(diffscale*cc), 0.75);
	m_diffusersR[2] = new Diffuser((int)(diffscale*dd), 0.625);
	m_diffusersR[3] = new Diffuser((int)(diffscale*e ), 0.625);



	//
	// Tapped delay section
	//

	m_tapDelay = new FixedDelay(44000);

	m_taps[0] = (int)(5  +  0.410 * m_delayLargest);
	m_taps[1] = (int)(5  +  0.300 * m_delayLargest);
	m_taps[2] = (int)(5  +  0.155 * m_delayLargest);
	m_taps[3] = (int)(5  +  0.000 * m_delayLargest);

	for(i = 0; i < FDNORDER; i++)
	{
	  m_tapsGains[i] = pow(m_alpha, (double)m_taps[i]);
	}

}




//------------------------------------------------------------------------------
Gigaverb::~Gigaverb()
//------------------------------------------------------------------------------
{
 delete m_inputDamper;

 for(int i = 0; i < FDNORDER; i++)
 {
  delete m_fdnDelays[i];
  delete m_fdnDampers[i];
  delete m_diffusersL[i];
  delete m_diffusersR[i];
 }

 delete m_tapDelay;
}


//------------------------------------------------------------------------------
void Gigaverb::Flush()
//------------------------------------------------------------------------------
{
	int i;

	m_inputDamper->Flush();
	
	for(i = 0; i < FDNORDER; i++)
	{
	 m_fdnDelays[i]->Flush();
	 m_fdnDampers[i]->Flush();
	 m_diffusersL[i]->Flush();
	 m_diffusersR[i]->Flush();
	 
	 m_d[i] = 0.0f;
	 m_u[i] = 0.0f;
	 m_f[i] = 0.0f;
	}
	
	m_tapDelay->Flush();
}



//------------------------------------------------------------------------------
void Gigaverb::Clear()
//------------------------------------------------------------------------------
{
int i, k;

 for(i = 0; i < FDNORDER; i++)
 {
  for(k = 0; k < m_fdnLengths[i]; k++) m_fdnDelays[i]->Write( 0.0f );
 }
}



//------------------------------------------------------------------------------
void Gigaverb::PerformDSPLoop(float *in1, float *in2, float *out1, float *out2, int n)
//------------------------------------------------------------------------------
{
	if (m_bypass)
	{
		// Bypass, so just copy input to output
		while(n--)
		{
			*out1++ = *in1++;
			*out2++ = *in2++;
		}
	}
	else
	{
    float out_l, out_r;
    float input;
    
    // DSP loop
		while(n--)
		{
			input = (*in1) + (*in2)*0.5;
			
			Do(input, &out_l, &out_r);
			
			*out1++ = out_l * m_wetLevel  +  (*in1) * m_dryLevel;
			*out2++ = out_r * m_wetLevel  +  (*in2) * m_dryLevel;
			
			in1++;
			in2++;
		}
	}
}





//------------------------------------------------------------------------------	
void Gigaverb::FdnMatrix(float *a, float *b)
//------------------------------------------------------------------------------
{
  const float dl0 = a[0], dl1 = a[1], dl2 = a[2], dl3 = a[3];

  b[0] = 0.5f*(+dl0 + dl1 - dl2 - dl3);
  b[1] = 0.5f*(+dl0 - dl1 - dl2 + dl3);
  b[2] = 0.5f*(-dl0 + dl1 - dl2 + dl3);
  b[3] = 0.5f*(+dl0 + dl1 + dl2 + dl3);
}	
	

//------------------------------------------------------------------------------
void Gigaverb::Do(float x, float *yl, float *yr)
//------------------------------------------------------------------------------
{
float z;
unsigned int i;
float lsum, rsum, sum, sign;


if(IS_NAN_FLOAT(x) || IS_DENORM_FLOAT(x) || fabsf(x) > 100000.0f)
{
	x = 0.0f;
}

z = m_inputDamper->Do(x);

z = m_diffusersL[0]->Do(z);

for(i = 0; i < FDNORDER; i++)
{
 m_u[i] = m_tapsGains[i] * m_tapDelay->Read(m_taps[i]);
}

m_tapDelay->Write(z);

for(i = 0; i < FDNORDER; i++)
{
 m_d[i] = m_fdnDampers[i]->Do( m_fdnGains[i] * m_fdnDelays[i]->Read( m_fdnLengths[i] ) );
}


sum = 0.0f;
sign = 1.0f;
for(i = 0; i < FDNORDER; i++)
{
 sum += sign * (m_tailLevel * m_d[i] + m_earlyLevel * m_u[i]);
 sign = -sign;
}
sum += x * m_earlyLevel;

lsum = sum;
rsum = sum;


  FdnMatrix(m_d, m_f);


for(i = 0; i < FDNORDER; i++)
{
 m_fdnDelays[i]->Write(m_u[i] + m_f[i]);
}

lsum = m_diffusersL[1]->Do(lsum);
lsum = m_diffusersL[2]->Do(lsum);
lsum = m_diffusersL[3]->Do(lsum);

rsum = m_diffusersR[1]->Do(rsum);
rsum = m_diffusersR[2]->Do(rsum);
rsum = m_diffusersR[3]->Do(rsum);

*yl = lsum;
*yr = rsum;
}



/*
 * This FDN reverb can be made smoother by setting matrix elements at the
 * diagonal and near of it to zero or nearly zero. By setting diagonals to zero
 * means we remove the effect of the parallel comb structure from the
 * reverberation.  A comb generates uniform impulse stream to the reverberation
 * impulse response, and thus it is not good. By setting near diagonal elements
 * to zero means we remove delay sequences having consequtive delays of the
 * similar lengths, when the delays are in sorted in length with respect to
 * matrix element index. The matrix described here could be generated by
 * differencing Rocchesso's circulant matrix at max diffuse value and at low
 * diffuse value (approaching parallel combs).
 *
 * Example 1:
 * Set a(k,k), for all k, equal to 0.
 *
 * Example 2:
 * Set a(k,k), a(k,k-1) and a(k,k+1) equal to 0.
 *
 * Example 3: The transition to zero gains could be smooth as well.
 * a(k,k-1) and a(k,k+1) could be 0.3, and a(k,k-2) and a(k,k+2) could
 * be 0.5, say.
 */

//
// Set Methods:
//

//------------------------------------------------------------------------------
void Gigaverb::SetRoomSize(double a)
//------------------------------------------------------------------------------
{
	unsigned int i;
	
	m_roomSizeNorm = a;
	a = a*a * this->m_roomSizeMax;
	
	if(a <= 1.0 || IS_NAN_FLOAT(a))
	{
		m_roomSize = 1.0;
	}
	else
	{
		m_roomSize = CLIP(a, 1.0f, m_roomSizeMax);
	}
	
	
	m_delayLargest = m_sampleRate * m_roomSize * 0.00294f;

	m_fdnLengths[0] = Utils::ff_round(1.000000f * m_delayLargest);
	m_fdnLengths[1] = Utils::ff_round(0.816490f * m_delayLargest);
	m_fdnLengths[2] = Utils::ff_round(0.707100f * m_delayLargest);
	m_fdnLengths[3] = Utils::ff_round(0.632450f * m_delayLargest);
	
	for(i = 0; i < FDNORDER; i++)
	{
		m_fdnGains[i] = -powf((float)m_alpha, m_fdnLengths[i]);
	}

	m_taps[0] = 5 + Utils::ff_round(0.410f * m_delayLargest);
	m_taps[1] = 5 + Utils::ff_round(0.300f * m_delayLargest);
	m_taps[2] = 5 + Utils::ff_round(0.155f * m_delayLargest);
	m_taps[3] = 5 + Utils::ff_round(0.000f * m_delayLargest);

	for(i = 0; i < FDNORDER; i++)
	{
		m_tapsGains[i] = powf((float)m_alpha, m_taps[i]);
	}
}

//------------------------------------------------------------------------------
void Gigaverb::SetReverbTime(double a)
//------------------------------------------------------------------------------
{
	float ga,gt;
	double n;
	unsigned int i;

  m_reverbTimeNorm = a;
  a = 0.1*(1.0f - a*a) + 360.0f * a*a; // quadratic scale
	m_reverbTime = CLIP(a, 0.1f, 360.0f);

	ga = 60.0f;
	gt = m_reverbTime;
	ga = powf(10.0f,-ga/20.0f);
	n = m_sampleRate*gt;
	m_alpha = (double)powf(ga, 1.0f/n);

	for(i = 0; i < FDNORDER; i++)
	{
		m_fdnGains[i] = -powf((float)m_alpha, m_fdnLengths[i]);
	}
}


//------------------------------------------------------------------------------
void Gigaverb::SetDamping(double a)
//------------------------------------------------------------------------------
{
	unsigned int i;

	m_fdnDamping = CLIP(a, 0.0f, 1.0f);
	
	for(i = 0; i < FDNORDER; i++)
	{
		m_fdnDampers[i]->Set(m_fdnDamping);
	}
}

//------------------------------------------------------------------------------
void Gigaverb::SetInputBandwidth(double a)
//------------------------------------------------------------------------------
{
	m_inputBandwidth = CLIP(a, 0.0f, 1.0f);
	m_inputDamper->Set(1.0f - m_inputBandwidth);
}

//------------------------------------------------------------------------------
void Gigaverb::SetDryLevel(double a)
//------------------------------------------------------------------------------
{
  m_dryLevelNorm = a;
  float db = (1.0f - a) * -90.0f;
	db = CLIP(db, -90.0f, 0.0f);
	m_dryLevel = DB_CO(db);
}

//------------------------------------------------------------------------------
void Gigaverb::SetWetLevel(double a)
//------------------------------------------------------------------------------
{
  m_wetLevelNorm = a;
  float db = (1.0f - a) * -90.0f;
	db = CLIP(db, -90.0f, 0.0f);
	m_wetLevel = DB_CO(db);  
}

//------------------------------------------------------------------------------
void Gigaverb::SetEarlyLevel(double a)
//------------------------------------------------------------------------------
{
  m_earlyLevelNorm = a;
  float db = (1.0f - a) * -90.0f;
	db = CLIP(db, -90.0f, 0.0f);
	m_earlyLevel = DB_CO(db);
}

//------------------------------------------------------------------------------
void Gigaverb::SetTailLevel(double a)
//------------------------------------------------------------------------------
{
  m_tailLevelNorm = a;
  float db = (1.0f - a) * -90.0f;
	db = CLIP(db, -90.0f, 0.0f);
	m_tailLevel = DB_CO(db);
}


//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
/*
  // Parameters set Methods:
  void SetRoomSize(double a);
  void SetReverbTime(double a);
  void SetDamping(double a);
  void SetInputBandwidth(double a);
  void SetDryLevel(double a);
  void SetWetLevel(double a);
  void SetEarlyLevel(double a);
  void SetTailLevel(double a);
*/
  
//
// Let's talk with Steinberg's VST functions...
//------------------------------------------------------------------------------
float Gigaverb::GetParameterValue(int index)
//------------------------------------------------------------------------------
{
// must return a value in the range 0..1
int i = index;

if(i < 0 || i > 7) return 0.0f;

  if(i == 0) return this->m_roomSizeNorm;
  if(i == 1) return this->m_reverbTimeNorm;
  
  if(i == 2) return this->m_fdnDamping;
  if(i == 3) return this->m_inputBandwidth;
  
  if(i == 4) return this->m_dryLevelNorm;
  if(i == 5) return this->m_wetLevelNorm;
  if(i == 6) return this->m_earlyLevelNorm;
  if(i == 7) return this->m_tailLevelNorm;

return 0.0f;
}

//------------------------------------------------------------------------------
void Gigaverb::SetParameterValue(int index, float a)
//------------------------------------------------------------------------------
{
int i = index;

if(i < 0 || i >= kGigaverbNumParams) return;

  if( i == kRoomSize       ) this->SetRoomSize(a);
  if( i == kReverbTime     ) this->SetReverbTime(a);
  if( i == kDamping        ) this->SetDamping(a);
  if( i == kInputBandwidth ) this->SetInputBandwidth(a);
  if( i == kDryLevel       ) this->SetDryLevel(a);
  if( i == kWetLevel       ) this->SetWetLevel(a);
  if( i == kEarlyLevel     ) this->SetEarlyLevel(a);
  if( i == kTailLevel      ) this->SetTailLevel(a);
}

//------------------------------------------------------------------------------
void Gigaverb::GetParameterDisplay (int index, char* text)
//------------------------------------------------------------------------------
{
  char buf[1024];
	float val;
	int i = index;
	
	// dB values
	if( i == kDryLevel   || i == kWetLevel ||
      i == kEarlyLevel || i == kTailLevel )
  {
   val = this->GetParameterValue(i);
   sprintf(text, "%.3f", CO_DB(val));
  }
  else // RoomSize, ReverbTime, Damping and Input BW
  {
   if( i == kRoomSize )   val = this->m_roomSize; else
   if( i == kReverbTime ) val = this->m_reverbTime; else
   val = this->GetParameterValue(i) * 100.0f; // percent
   
	 sprintf(text, "%.3f", val);
  }	
}



//------------------------------------------------------------------------------
const char* Gigaverb::GetParameterName(int index)
//------------------------------------------------------------------------------
{
static const char names[][16] = {
  {"Size"},
  {"Time"},
  {"Damp"},
  {"Input BW"},
  {"Dry"},
  {"Wet"},
  {"Early"},
  {"Tail"}
};

if (index < 0 || index >= kGigaverbNumParams) return "error";

return names[index];
}


//------------------------------------------------------------------------------
const char* Gigaverb::GetParameterLabel(int index)
//------------------------------------------------------------------------------
{
static const char names[][16] = {
  {"meters"},
  {"seconds"},
  {"%"},
  {"%"},
  {"dB"},
  {"dB"},
  {"dB"},
  {"dB"}
};

if (index < 0 || index >= kGigaverbNumParams) return "error";

return names[index];
}
