/*
 Original code From "z_dsp.h" -- the main header file for all DSP objects
 copyright 1997-2003 Cycling '74
*/ 


#ifndef __Denorm_h__
#define __Denorm_h__


#define _WINDOWS_ 1


#ifdef _WINDOWS_ 
 #define WIN_VERSION 1
#endif

// [...]

// macro loop for checking for NAN/INF
// note: this may be platform-dependent

#define NAN_MASK 0x7F800000

#define NAN_CHECK(n,o) \
while (n--) { if ((*(o) & NAN_MASK) == NAN_MASK) *(o) = 0; (o)++; } // now post inc/dec -Rd jun 05

#define IS_DENORM_FLOAT(v)		((((*(unsigned long *)&(v))&0x7f800000)==0)&&((v)!=0.f))
#define IS_DENORM_DOUBLE(v)		((((((unsigned long *)&(v))[1])&0x7fe00000)==0)&&((v)!=0.))

#define IS_NAN_FLOAT(v)			(((*(unsigned long *)&(v))&0x7f800000)==0x7f800000)
#define IS_NAN_DOUBLE(v)		(((((unsigned long *)&(v))[1])&0x7fe00000)==0x7fe00000)

#define IS_DENORM_NAN_FLOAT(v)		(IS_DENORM_FLOAT(v)||IS_NAN_FLOAT(v))
#define IS_DENORM_NAN_DOUBLE(v)		(IS_DENORM_DOUBLE(v)||IS_NAN_DOUBLE(v))

// currently all little endian processors are x86
#if defined(WIN_VERSION) || (defined(MAC_VERSION) && TARGET_RT_LITTLE_ENDIAN)
 #define DENORM_WANT_FIX		1
#endif

#ifdef DENORM_WANT_FIX

  #define FIX_DENORM_FLOAT(v)		((v)=IS_DENORM_FLOAT(v)?0.f:(v))
  #define FIX_DENORM_DOUBLE(v)	((v)=IS_DENORM_DOUBLE(v)?0.f:(v))
  
  #define FIX_DENORM_NAN_FLOAT(v)		((v)=IS_DENORM_NAN_FLOAT(v)?0.f:(v))
  #define FIX_DENORM_NAN_DOUBLE(v)	((v)=IS_DENORM_NAN_DOUBLE(v)?0.:(v))

#else

  #define FIX_DENORM_FLOAT(v)
  #define FIX_DENORM_DOUBLE(v)
  
  #define FIX_DENORM_NAN_FLOAT(v)
  #define FIX_DENORM_NAN_DOUBLE(v)

#endif


// [...]

#endif

