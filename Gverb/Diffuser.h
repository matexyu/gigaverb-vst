/*
 Original code by Olaf Matthes
 C++ porting by Mathieu Bosi
*/

#ifndef __Diffuser_h__
#define __Diffuser_h__

#include "Utils.h"

class Diffuser {

public:

  float m_coeff;
  int m_idx;
  int m_size;
  float *m_buf;	
	
	
Diffuser(int size, float coeff)
{
int i;

 m_size  = size;
 m_coeff = coeff;
 m_idx = 0;
 m_buf = new float[size]; 
 
 for (i = 0; i < size; i++) m_buf[i] = 0.0;
}

~Diffuser()
{
 delete m_buf;
}

void Flush()
{
 memset(m_buf, 0, m_size * sizeof(float));
}


float Do(float x)
{
float y,w;

 w = x - m_buf[m_idx] * m_coeff;

 FIX_DENORM_NAN_FLOAT(w);

 y = m_buf[m_idx] + w*m_coeff;

 m_buf[m_idx] = w;
 m_idx = (m_idx + 1) % m_size;

return( y );
}

};



#endif

