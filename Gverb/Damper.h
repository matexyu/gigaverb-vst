/*
 Original code by Olaf Matthes
 C++ porting by Mathieu Bosi
*/

#ifndef __Damper_h__
#define __Damper_h__

#include "Utils.h"

class Damper {
	
public:

float m_damping;
float m_delay;
	

Damper(float damping)
{
 m_damping = damping;
 m_delay = 0.0;
}

~Damper()
{
}

void Flush()
{
  m_delay = 0.0f;
}

void Set(float damping)
{ 
	m_damping = damping;
} 
  
float Do(float x)
{ 
float y;

 y = x*(1.0 - m_damping) + m_delay * m_damping;
 m_delay = y;
 
return(y);
}

};


#endif
